#! /bin/sh

# this script is designed to work on Ubuntu 16.04 LTS (Server 64-bit)
# installation script follows instructions in https://docs.moodle.org/36/en/Step-by-step_Installation_Guide_for_Ubuntu, with some minor tweaks

export DEBIAN_FRONTEND=noninteractive
export LC_TYPE="UTF-8"
export LANG="en-US.UTF-8"
export LC_ALL="C.UTF-8"

MOODLE_USER="moodledude"
MOODLE_PASS="passwordformoodledude"

add-apt-repository ppa:ondrej/php

apt-get update

apt-get install -y apache2 mariadb-server php7.0 libapache2-mod-php7.0 vim

apt-get install -y php7.0-pspell php7.0-curl php7.0-gd php7.0-intl php7.0-mysql php7.0-xml php7.0-xmlrpc php7.0-ldap php7.0-zip php7.0-soap php7.0-mbstring

apt-get install -y graphviz aspell ghostscript clamav 

service apache2 restart

apt-get install git-core

cd /opt
git clone -b MOODLE_36_STABLE git://git.moodle.org/moodle.git

cp -R /opt/moodle /var/www/html/
mkdir /var/moodledata
chown -R www-data /var/moodledata
chmod -R 777 /var/moodledata
chmod -R 777 /var/www/html/moodle

cat <<- EOF > /etc/mysql/mariadb.conf.d/99-moodle.cnf
[mysqld]
default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 4096
collation-server = utf8_general_ci
character-set-server = utf8
EOF

service mysql restart

mysql_secure_installation <<- EOF

n
y
n
y
y
EOF

mysql <<- EOF
CREATE DATABASE moodle DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create user '$MOODLE_USER'@'localhost' IDENTIFIED BY '$MOODLE_PASS';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO $MOODLE_USER@localhost IDENTIFIED BY '$MOODLE_PASS';
EOF

echo "Open your browser and go to http://IP.ADDRESS.OF.SERVER:4567/moodle"
echo "\nIf installed in VirtualBox VM try http://localhost:4567/moodle\n
"
echo "\nTo configure Database, select MariaDB\n"
echo "Moodle User is $MOODLE_USER"
echo "Moodle User pass is $MOODLE_PASS"

echo "\nDon't forget to revert permission on /var/www/html/moodle"
echo "# chmod -R 755 /var/www/html/moodle"
