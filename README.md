# moodle-ubuntu-vm

**Scripts to install Moodle Server in Ubuntu VM**

Provision Ubuntu 16.04 LTS Server 64-bit (virtual) machine, then clone this repo:

`$ git clone https://gitlab.com/krisocc/moodle-ubuntu-vm`

`$ cd moodle-ubuntu-vm`

Run the installation script as superuser:

`$ sudo /bin/sh install_moodle.sh`

Alternatively, you can use Vagrant + VirtualBox to automatically provision this VM.

Install VirtualBox from https://www.virtualbox.org/wiki/Downloads

Install Vagrant from https://www.vagrantup.com/downloads.html

Clone the repo:

`$ git clone https://gitlab.com/krisocc/moodle-ubuntu-vm`

`$ cd moodle-ubuntu-vm`

Summon the VM:

`$ vagrant up`

Go to Moodle Server website: http://IP.ADDRESS.OF.SERVER/moodle
or http://localhost:4567/moodle if you use Vagrant provisioned VM.

Enter following configuration items:

*  Language of choice
*  Data Directory: **/var/moodledata**
*  Database Driver Type: **mariadb/native**
*  Database user: **moodledude**
*  Database password: **passwordformoodledude**

Follow with further Installation options, including Admin password and Site Name...
